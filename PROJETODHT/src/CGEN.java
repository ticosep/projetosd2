import java.util.Date;
import java.util.Random;

public class CGEN {

    public static Long getCode(){
        Random rand = new Random();
        rand.setSeed(new Date().getTime());
        byte[] buff = new byte[16];
        rand.nextBytes(buff);
        return getCode(buff);
    }
    public static Long getCode(byte[] data){
        return Long.parseUnsignedLong(MD5.toHexString(MD5.computeMD5(data)).substring(0,15),16);

    }
    public static int getPort(){
        Random rand = new Random();
        rand.setSeed(new Date().getTime());
        return rand.nextInt(22000)+8000;

    }
}
