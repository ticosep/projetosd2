import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UserInterface {
	private JButton btnJoin = new JButton("Join");
	private JButton btnRetrieve = new JButton("Retrieve");
	private JButton btnLeave = new JButton("Leave");
	private JButton btnStore = new JButton("Store");

	
	private JLabel infos = new JLabel();
	private Node node = new Node();
	private String ip = node.getIp() ;
    private String port1 = Integer.toString(node.getPORT1());
    private String port2 = Integer.toString(node.getPORT2());
   
	// cria o listner para os botoes
	private ActionListener act = new ActionListener() { 
        public void actionPerformed(ActionEvent e) {
        	
        	long CODE = CGEN.getCode();
        	String cmd = e.getActionCommand().toUpperCase() + "#" + Long.toString(CODE) +"#" + port1 + "#" + port2 + "#" + ip;
        	
        	switch (e.getActionCommand().toUpperCase()) {
             case "JOIN":
                  	
             	node.joinTerminal();
             	JOptionPane.showMessageDialog(null, "Node adicionado com sucesso sob o CODE: " + CODE,"JOIN", JOptionPane.INFORMATION_MESSAGE);
            case "LEAVE":
                node.leaveTerminal();
            case "STORE":
                node.storeTerminal();
                
            case "RETRIEVE":
                node.retrieveTerminal();
            
        	}
                
        	
    }};
	
    //add listner para click dos botoes
	public UserInterface() {
		this.btnJoin.addActionListener(this.act);
		this.btnStore.addActionListener(this.act);
		this.btnRetrieve.addActionListener(this.act);
		this.btnLeave.addActionListener(this.act);
		
		
		this.infos.setText("IP: " + this.ip + " PORT1: " + this.port1 + " PORT2: " + this.port2);
	}

	// carrega os elementos na janela
	public void loadResources () throws IOException {
		
		
		java.net.URL url = this.getClass().getResource("chord.png");
		BufferedImage myPicture = ImageIO.read(new File(url.getPath()));
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
						
		JPanel painel = new JPanel();
		painel.add(this.infos);
		painel.add(this.btnJoin);
		painel.add(this.btnRetrieve);
		painel.add(this.btnLeave);
		painel.add(this.btnStore);
		painel.add(picLabel);
        
		JFrame janela = new JFrame("DHT");
		janela.setSize(300, 300);
		janela.add(painel);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		janela.setVisible(true);
		
		this.node.waitMessage();
      }

}
