import java.io.*;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Node {
    //DEFINIDAS AS PORTAS ENTRE 8000 ATÉ 30000 PARA OS NODES USAREM
    final int MIN_PORT_NUMBER = 8000;
    final int MAX_PORT_NUMBER = 30000;
    //CAMINHO DO DIRETORIO QUE O NODE GUARDA OS ARQUIVOS
    String DIR;
    //IDENTIFICADOR DE M-BITS (NO CASO AQUI SÃO 15 DIGITOS)
    private Long CODE;
    //DOIS NUMEROS DE PORTAS
    //UMA PARA CONTROLE OUTRA PARA DADOS, ACREDITO QUE NO CASO DESTE PROJETO SEJA SUFICIENTE
    private int PORT1;
    private int PORT2;
    //O FILE DO DIRETORIO
    //NOTA: COMO O NOME DO REPOSITÓRIO É SEMPRE O MESMO
    // A UNICA EXIGENCIA É QUE UTILIZAR UM PROCESSO PARA CADA PASTA
    private File repository;
    //STRING QUE GUARDA OS DADOS DOS VIZINHOS DE ANTERIOR E SUCESSOR
    //FORMATO: CODE#PORT1#PORT2#IP
    private String back;
    private String front;
    //IP DO NODE, NESTA APLICAÇÃO UTILIZA-SE APENAS O LOCALHOST
    private String ip;

    //CONSTRUTOR DEFINE IDENTIFICADOR, PORTAS,CRIA O DIRETORIO, IP E INFORMA OS DADOS AO CRIAR
    public Node() {
        int p = CGEN.getPort();
        System.out.println(p + "");
        while (!isPortOccupied(p))
            p++;
        setPORT1(p);
        int p2 = CGEN.getPort();
        System.out.println(p2 + "");
        while (!isPortOccupied(p2))
            p2++;
        setPORT2(p2);
        //SEM NENHUMA VERIFICAÇÃO SE O CÓDIGO ESTÁ SENDO UTILIZADO, SÃO 15 DIGITOS, POUCO PROVÁVEL SE REPETIR
        setCODE(CGEN.getCode());
        setDirectory();
        //COMO A APLICAÇÃO É LOCAL PARA O PROJETO UTILIZA-SE O LOCALHOST MESMO
        setIp("127.0.0.1");
        back = null;
        front = null;
        System.out.println("NODE CREATED\nPORT1:" + getPORT1() + "\nPORT2:" + getPORT2() + "\nCODE:" + getCODE() + "\nREPOSITORY:" + DIR);
    }

  
    //FUNÇÃO "SERVIDOR", FICA ESPERANDO O CONTATO DE OUTROS NODES
    public void waitMessage() {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(this.getPORT1());
        } catch (IOException e) {
            e.printStackTrace();
        }


        while (true) {
            Socket socket = null;
            try {
                assert serverSocket != null;
                socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
            Socket finalSocket = socket;
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        //RECEBE A STRING DE COMANDO NA PORT1 E PASSA PARA O HANDLER
                        InputStreamReader isrServer = new InputStreamReader(finalSocket.getInputStream());
                        BufferedReader serverReader = new BufferedReader(isrServer);
                        String cmd = serverReader.readLine();
                        cmdHandler(cmd);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (finalSocket != null)
                                finalSocket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
            thread.start();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    //AQUI PARA LIDAR COM OS COMANDOS VINDOS DE FORA
    public void cmdHandler(String cmd) {
        String[] op = cmd.split("#");
        String OP = op[0].toUpperCase();
        switch (OP) {
            case "JOIN":
                joinHandler(cmd);
                break;
            case "JOIN_OK":
                joinOKHandler(cmd);
                break;
            case "NEW_NODE":
                newNodeHandler(cmd);
                break;
            case "LEAVE":
                leaveHandler(cmd);
                break;
            case "NODE_GONE":
                nodeGoneHandler(cmd);
                break;
            case "STORE":
                storeHandler(cmd);
                break;
            case "RETRIEVE":
                retrieveHandler(cmd);
                break;
            case "OK":
                OKHandler(cmd);
                break;
            case "NOT_FOUND":
                notFoundHandler(cmd);
                break;
            case "TRANSFER":
                transferHandler(cmd);
                break;
            case "":
                break;
            default:
                OP = "";
                break;

        }

    }


    //BEGIN FUNÇOES DO HANDLER

    //JOIN É UM NÓ QUE ESTÁ TENTANDO ENTRAR NA REDE
    // VERIFICAR SE EU DEVO LIDAR COM ELE
    //SE SIM ELE É MEU ANTECESSOR
    // MANDO PRA ELE MEUS DADOS E MEU ATUAL ANTECESSOR
    // MANDO OS DADOS(TRANSFER) QUE SÃO DELE
    //ATUALIZO MEU ANTECESSOR
    public void joinHandler(String cmd) {
        //[0]JOIN#[1]CODE#[2]PORT1#[3]PORT2#[4]IP
        String[] op = cmd.split("#");
        Long FCODE = Long.parseUnsignedLong(op[1]);
        if (AMIALONE()) {
            String neighbor = op[1] + "#" + op[2] + "#" + op[3] + "#" + op[4];
            setFront(neighbor);
            setBack(neighbor);

        }

    }

    public void joinOKHandler(String cmd) {

    }

    public void newNodeHandler(String cmd) {

    }

    public void leaveHandler(String cmd) {

    }

    public void nodeGoneHandler(String cmd) {

    }

    public void storeHandler(String cmd) {

    }

    public void retrieveHandler(String cmd) {

    }

    public void OKHandler(String cmd) {

    }

    public void notFoundHandler(String cmd) {

    }

    public void transferHandler(String cmd) {

    }

    //END FUNÇOES DO HANDLER


    //AQUI PARA LIDAR COM OS COMANDOS VINDOS DO PROPRIO NÓ VIA TERMINAL
    public void cmdTerminal() {
        String str = "";
        Scanner scanner = new Scanner(System.in);
        while (!str.equals("exit")) {
            str = scanner.nextLine();
            switch (str.toUpperCase()) {
                case "JOIN":
                    joinTerminal();
                    break;
                case "RETRIEVE":
                    retrieveTerminal();
                    break;
                case "STORE":
                    storeTerminal();
                    break;
                case "LEAVE":
                    leaveTerminal();
                    break;

                default:
                    break;
            }

        }
    }

    //BEGIN FUNÇOES DO TERMINAL
    public void joinTerminal() {

    }

    public void retrieveTerminal() {

    }

    public void storeTerminal() {

    }

    public void leaveTerminal() {

    }
    //END FUNCOES DO TERMINAL

    //UTILIZAR PARA ENVIAR COMANDOS
    public void SENDER(String cmd, String node) {
        //FORMATO: CODE#PORT1#PORT2#IP
        String[] n = node.split("#");
        Socket socket = null;
        try {
            socket = new Socket(n[3], Integer.parseInt(n[1]));
            OutputStream os = socket.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeBytes(cmd + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String me() {
        return this.getCODE() + "#" + this.getPORT1() + "#" + getPORT2() + "#" + getIp();
    }

    //CONSTRUCTORS
    public long getCODE() {
        return CODE;
    }

    public void setCODE(long CODE) {
        this.CODE = CODE;
    }

    public int getPORT1() {
        return PORT1;
    }

    public void setPORT1(int PORT) {
        this.PORT1 = PORT;
    }

    public int getPORT2() {
        return PORT2;
    }

    public void setPORT2(int PORT2) {
        this.PORT2 = PORT2;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public String getFront() {
        return front;
    }

    public void setFront(String front) {
        this.front = front;
    }

    boolean isPortOccupied(int port) {
        if (port < MIN_PORT_NUMBER || port > MAX_PORT_NUMBER) {
            throw new IllegalArgumentException("Invalid start port: " + port);
        }
        ServerSocket ss = null;
        DatagramSocket ds = null;
        try {
            ss = new ServerSocket(port);
            ss.setReuseAddress(true);
            ds = new DatagramSocket(port);
            ds.setReuseAddress(true);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ds != null) {
                ds.close();
            }

            if (ss != null) {
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

    //VERIFICA SE É O UNICO
    public boolean AMIALONE() {
        return (front == null && back == null);
    }

    //VERIFICA SE É O ULTIMO
    public boolean AMILAST() {
        if (!AMIALONE()) {
            return (Long.parseUnsignedLong(front.split("#")[0]) < getCODE());
        }
        return true;
    }

    //VERIFICA SE É O PRIMEIRO
    public boolean AMIFIRST() {
        if (!AMIALONE()) {
            return (Long.parseUnsignedLong(back.split("#")[0]) > getCODE());
        }
        return true;
    }

    public void setDirectory() {
        //String str = null;
        try {
            DIR = new File(".").getCanonicalPath() + File.separator + "systemRepository";
        } catch (IOException e) {
            e.printStackTrace();
        }
        repository = new File(DIR);
        if (!repository.exists()) {
            if (repository.mkdirs())
                System.out.println("Directory created");
            else
                System.out.println("Directory not created");

        } else {
            System.out.println("repository already created");
        }

    }
 }
